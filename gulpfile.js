var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');

//Combine sass into css
gulp.task('sass', function() {
    return gulp.src('src/assets/scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('src/assets/css'))
        .pipe(browserSync.stream());
});

//Combine js into js
gulp.task('js', function() {
    return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js', 'node_modules/jquery/dist/jquery.min.js', 'node_modules/tether/dist/js/tether.min.js'])
        .pipe(gulp.dest('src/assets/js'))
        .pipe(browserSync.stream());
});

gulp.task('serve', gulp.series('sass', function () {
    browserSync.init({
        server: './src'
    });

    gulp.watch('src/assets/scss/*.scss').on('change', browserSync.reload);
    gulp.watch('src/*.html').on('change', browserSync.reload);
}));

gulp.task('default', gulp.parallel(['js', 'serve']));